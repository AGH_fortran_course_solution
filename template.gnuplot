unset grid
set terminal pdf
set output 'wykres[kind].pdf'
set key box top left
set multi
set logscale x
set logscale y
set key opaque
set key box
set key width 1 height 0.5 font "Arial, 14"
set style data lines
set termopt enhanced
set xlabel "N" font "Arial, 14"
set ylabel "multiplication time" font "Arial, 14"
set xtics font "Arial, 14"
set ytics font "Arial, 14"
set termoption dashed
plot   'naiv_[kind]'  title 'naive',\
       'bett_[kind]'  title 'bett',\
       'dot_[kind]'   title 'dot',\
       'mat_[kind]'   title 'mat',\
       'bett2_[kind]' title 'bett2',\
       'block_[kind]' title 'block'
unset multi